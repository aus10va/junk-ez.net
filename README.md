# BUILD 1

## Description
The repository for this project is empty, if you see this then the build was successful
You can get started by cloning the repository or start adding files to it with one of the following options.

### Command line instructions
You can also upload existing files from your computer using the instructions below.<br><br>

>> Git global setup <br>
>> git config --global user.name "YOUR_NAME" <br>
>> git config --global user.email "your@email" <br>

### Create a new repository

>> git clone https://gitlab.com/<git_user_name>/<git_repo>.git <br>
>> cd git_repo <br>
>> git switch -c main <br>
>> touch README.md <br>
>> git add README.md <br>
>> git commit -m "add README" <br>
>> git push -u origin main <br>
>> Push an existing folder <br>
>> cd existing_folder <br>
>> git init --initial-branch=main <br>
>> git remote add origin  https://gitlab.com/<git_user_name>/<git_repo>.git <br>
>> git add . <br>
>> git commit -m "Initial commit" <br>
>> git push -u origin main <br>

### Push an existing Git repository <br>
>> cd existing_repo <br>
>> git remote rename origin old-origin <br>
>> git remote add origin  https://gitlab.com/<git_user_name>/<git_repo>.git <br>
>> git push -u origin --all <br>
>> git push -u origin --tags <br>

Create a venv
Manual install required for 
pip install requests
pip install watchdog
pip install git-lab