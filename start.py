import pkg_resources
import subprocess
import os
import time

# MANUAL INSTALL
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from pyvirtualdisplay import Display

# Create a virtual display
display = Display(visible=0, size=(800, 600))
display.start()
def generate_ssh_keys():
    ssh_key_dir = os.path.expanduser("~/.ssh")
    ssh_key_path = os.path.join(ssh_key_dir, "id_rsa")

    # Check if SSH keys already exist
    if os.path.exists(ssh_key_path):
        print("SSH keys already exist.")
        return

    # Generate SSH keys
    subprocess.run(["ssh-keygen", "-t", "rsa", "-b", "4096", "-C", "your_email@example.com", "-f", ssh_key_path])

    print("SSH keys generated successfully.")


def store_ssh_keys_in_gitlab(repository_url):
    ssh_key_path = os.path.expanduser("~/.ssh/id_rsa.pub")

    # Check if the SSH public key file exists
    if not os.path.exists(ssh_key_path):
        print("SSH public key file not found.")
        return

    # Get the SSH public key
    with open(ssh_key_path, "r") as file:
        ssh_public_key = file.read().strip()

    # Store the SSH public key in GitLab
    subprocess.run(["git", "config", "--global", "user.email", "your_email@example.com"])
    subprocess.run(["git", "config", "--global", "user.name", "Your Name"])
    subprocess.run(["git", "clone", repository_url])
    repository_name = repository_url.split("/")[-1].split(".")[0]
    repository_path = os.path.join(os.getcwd(), repository_name)  # Updated line
    os.chdir(repository_path)
    subprocess.run(["git", "remote", "set-url", "origin", repository_url])
    subprocess.run(["git", "lab", "token", "create"])
    subprocess.run(["git", "lab", "project", "edit", "--ssh-key", ssh_public_key])

    print("SSH keys stored in GitLab successfully.")


# Specify the URL of the GitLab repository
repository_url = "git@gitlab.com:aus10va/junk-ez.net.git"

# Generate SSH keys
generate_ssh_keys()

# Store SSH keys in GitLab
store_ssh_keys_in_gitlab(repository_url)

# Set the directory to monitor
directory_to_watch = './junk-ez.net/junk-ez.net'

# Set the git repository path
repository_path = './junk-ez.net/junk-ez.net/.git'

class GitPushEventHandler(FileSystemEventHandler):
    def on_modified(self, event):
        if not event.is_directory:
            file_path = event.src_path
            print(f'Changes detected in {file_path}. Running git push...')

            # Run git push command
            subprocess.run(['git', '-C', repository_path, 'push'])

# Create an instance of the event handler
event_handler = GitPushEventHandler()

# Create an observer to watch for file system events
observer = Observer()
observer.schedule(event_handler, directory_to_watch, recursive=True)
observer.start()

try:
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    observer.stop()

observer.join()

def create_gitlab_gitignore():
    gitignore_content = '''# GitLab specific
.gitlab-ci.yml
.gitlab/
.gitlab-*

# GitLab Pages
public/

# GitLab artifacts
*.zip
*.gz
*.tar
*.tar.*
*.7z
*.rar
*.jar
*.war
*.ear
*.sar
*.xpi
*.xar
*.dll
*.exe
*.lib
*.so
*.dylib
*.dll.a
*.o
*.obj

# GitLab Workhorse
*.tar.*
*.mwb

# GitLab Dependency Proxy
/packages/

# GitLab Container Registry
/container/
*.dockerfile

# GitLab Registry
*.tar.gz
*.img
*.ova
*.qcow2
*.vdi
*.vhd
*.vhdx
*.vmdk
*.box

# GitLab Pages (https://about.gitlab.com/stages-devops-lifecycle/pages/)
public/
'''

    with open('.gitignore', 'w') as file:
        file.write(gitignore_content)

# Call the function to create the .gitignore file for GitLab
create_gitlab_gitignore()

# END PIPELINE SCRIPT AND START MAIN PROGRAM
def install_k3s():
    command = 'curl -sfL https://get.k3s.io | sh -s - --disable servicelb --disable traefik --disable local-storage'

    try:
        completed_process = subprocess.run(command, shell=True, capture_output=True)
        if completed_process.returncode == 0:
            print('K3s installation completed successfully.')
            return True
        else:
            print(f'Error: K3s installation failed. Command returned non-zero exit status {completed_process.returncode}.')
            print(f'Error output:\n{completed_process.stderr.decode()}')
            return False
    except subprocess.CalledProcessError as e:
        print(f'Error: K3s installation failed. {e}')
        return False

def check_k3s_installed():
    command = 'k3s --version'
    try:
        completed_process = subprocess.run(command, shell=True, capture_output=True)
        if completed_process.returncode == 0:
            print('K3s is already installed.')
            return True
        else:
            return False
    except subprocess.CalledProcessError as e:
        return False


def configure_k3s_and_dashboard():
    print('Configuring K3s and the dashboard...')
    try:
        subprocess.run('kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.4.1/aio/deploy/recommended.yaml', shell=True, check=True, capture_output=True)
        print('Dashboard configured successfully.')
        
        print('Creating admin user profile...')
        subprocess.run('kubectl create -f admin-user.yaml', shell=True, check=True, capture_output=True)
        print('Admin user profile created successfully.')
        
        print('To access the dashboard, run: kubectl proxy')
    except subprocess.CalledProcessError as e:
        print('Error: Failed to configure the dashboard.')
        if e.stderr:
            print(f'Error output:\n{e.stderr.decode()}')
        else:
            print(f'Error output:\n{e.output.decode()}')

def setup_k3s_and_dashboard():
    if not check_k3s_installed():
        print('K3s is not installed. Installing K3s...')
        if install_k3s():
            configure_k3s_and_dashboard()
    else:
        configure_k3s_and_dashboard()

def create_admin_user_yaml():
    yaml_content = '''
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kube-system

---

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kube-system
'''

    with open('admin-user.yaml', 'w') as file:
        file.write(yaml_content)

# Call the function to create the admin-user.yaml file
create_admin_user_yaml()

# Call the function to setup K3s and configure the dashboard
setup_k3s_and_dashboard()


def create_postgres_admin_user():
    username = "admin"
    password = "password"

    # Create the admin user
    create_user_command = f'psql -c "CREATE USER {username} WITH SUPERUSER CREATEDB CREATEROLE PASSWORD \'{password}\';"'
    subprocess.run(create_user_command, shell=True, check=True)

    print(f"PostgreSQL admin user '{username}' created.")

def create_postgres_database():
    database_name = "mydatabase"
    username = "admin"

    # Create the database
    create_database_command = f'psql -c "CREATE DATABASE {database_name} OWNER {username};"'
    subprocess.run(create_database_command, shell=True, check=True)

    print(f"PostgreSQL database '{database_name}' created.")

# Call the function to create the admin user and database
create_postgres_admin_user()
create_postgres_database()

def create_postgres_admin_user():
    username = "admin"
    password = "password"

    # Create the admin user
    create_user_command = f'psql -c "CREATE USER {username} WITH SUPERUSER CREATEDB CREATEROLE PASSWORD \'{password}\';"'
    subprocess.run(create_user_command, shell=True, check=True)

    print(f"PostgreSQL admin user '{username}' created.")

def create_postgres_database():
    database_name = "mydatabase"
    username = "admin"

    # Create the database
    create_database_command = f'psql -c "CREATE DATABASE {database_name} OWNER {username};"'
    subprocess.run(create_database_command, shell=True, check=True)

    print(f"PostgreSQL database '{database_name}' created.")

def configure_postgres_for_k3s():
    # Get the K3s cluster's PostgreSQL pod name
    get_pod_command = "kubectl get pods -n kube-system -l 'app=k3s,component=server' -o jsonpath='{.items[0].metadata.name}'"
    completed_process = subprocess.run(get_pod_command, shell=True, capture_output=True, text=True)
    if completed_process.returncode == 0:
        pod_name = completed_process.stdout.strip()
        print(f"K3s PostgreSQL pod name: {pod_name}")

        # Configure PostgreSQL for K3s
        configure_postgres_command = f'kubectl exec -n kube-system {pod_name} -- sh -c "psql -U postgres -c \\"ALTER SYSTEM SET listen_addresses = \'*\';\\" && systemctl restart k3s"'
        subprocess.run(configure_postgres_command, shell=True, check=True)

        print("PostgreSQL configured for K3s.")
    else:
        print("Failed to retrieve K3s PostgreSQL pod name.")

# Check if admin-user.yaml already exists
if not os.path.exists("admin-user.yaml"):
    # Call the function to create the admin user and database
    create_postgres_admin_user()
    create_postgres_database()

# Call the function to configure PostgreSQL for K3s
configure_postgres_for_k3s()
# Function to read the errors from each install on a server and debug on the fly

#def deploy_k3s_to_git():
#    git_repo_url = '<your-git-repo-url>'  # Replace with your Git repository URL
#    git_repo_dir = '<local-git-repo-dir>'  # Replace with the local directory where you want to clone the repository

    # Clone the Git repository
#    subprocess.run(['git', 'clone', git_repo_url, git_repo_dir])

    # Copy K3s deployment files to the Git repository directory
#   k3s_files_dir = '<k3s-files-dir>'  # Replace with the directory containing your K3s deployment files
#    subprocess.run(['cp', '-r', k3s_files_dir, git_repo_dir])

    # Commit and push changes to Git
#    commit_message = '<commit-message>'  # Replace with your desired commit message
#    subprocess.run(['git', 'add', '.'], cwd=git_repo_dir)
#    subprocess.run(['git', 'commit', '-m', commit_message], cwd=git_repo_dir)
#    subprocess.run(['git', 'push'], cwd=git_repo_dir)

#    print('K3s deployment files deployed to Git successfully.')

# Call the function to deploy K3s to Git
#deploy_k3s_to_git()

# Stop the virtual display
display.stop()

# EOF
